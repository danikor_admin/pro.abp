﻿using Volo.Abp.AspNetCore.Mvc.UI.Theming;
using Volo.Abp.DependencyInjection;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton
{
    [ThemeName("Lepton")]
	public class LeptonTheme : ITransientDependency, ITheme
	{
		public const string Name = "Lepton";

		public virtual string GetLayout(string name, bool fallbackToDefault = true)
		{
			if (name != null)
			{
				if (name == "Application")
				{
					return "~/Themes/Lepton/Layouts/Application/Default.cshtml";
				}
				if (name == "Account")
				{
					return "~/Themes/Lepton/Layouts/Account/Default.cshtml";
				}
				if (name == "Empty")
				{
					return "~/Themes/Lepton/Layouts/Empty/Default.cshtml";
				}
			}
			if (!fallbackToDefault)
			{
				return null;
			}
			return "~/Themes/Lepton/Layouts/Application/Default.cshtml";
		}
	}
}
