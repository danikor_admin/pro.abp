﻿namespace Volo.Abp.LanguageManagement
{
    public static class LanguageConsts
	{
		public static int MaxCultureNameLength { get; set; }

		public static int MaxDisplayNameLength { get; set; }

		public static int MaxFlagIconLength { get; set; }

		public static readonly int MaxUiCultureNameLength;

		static LanguageConsts()
		{
			MaxCultureNameLength = 10;
			MaxUiCultureNameLength = MaxCultureNameLength;
			MaxDisplayNameLength = 32;
			MaxFlagIconLength = 48;
		}
	}
}
