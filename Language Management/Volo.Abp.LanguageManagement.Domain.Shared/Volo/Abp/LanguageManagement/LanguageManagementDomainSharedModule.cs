﻿using Volo.Abp.LanguageManagement.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Validation;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.LanguageManagement
{
    [DependsOn(
		typeof(AbpLocalizationModule),
		typeof(AbpValidationModule)
	)]
	public class LanguageManagementDomainSharedModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
            base.Configure<AbpVirtualFileSystemOptions>(options =>
			{
				options.FileSets.AddEmbedded<LanguageManagementDomainSharedModule>();
			});
			base.Configure<AbpLocalizationOptions>(options=>
			{
				options.Resources
				.Add<LanguageManagementResource>("en")
				.AddBaseTypes(typeof(AbpValidationResource))
				.AddVirtualJson("/Volo/Abp/LanguageManagement/Localization/Resources");
			});
		}
	}
}
