﻿using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.TextTemplateManagement.Localization;

namespace Volo.Abp.TextTemplateManagement
{
    public abstract class TextTemplateManagementController : AbpController
	{
		protected TextTemplateManagementController()
		{
			base.LocalizationResource = typeof(TextTemplateManagementResource);
		}
	}
}
