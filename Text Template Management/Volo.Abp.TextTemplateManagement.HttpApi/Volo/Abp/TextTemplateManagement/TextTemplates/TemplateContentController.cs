﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.AspNetCore.Mvc;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    [Route("api/text-template-management/template-contents")]
	[RemoteService(true, Name = TextTemplateManagementRemoteServiceConsts.RemoteServiceName)]
	[Area("textTemplateManagement")]
	[ControllerName("TextTemplateContents")]
	[Authorize("TextTemplateManagement.TextTemplates")]
	public class TemplateContentController : AbpController, ITemplateContentAppService, IRemoteService, IApplicationService
	{
		private readonly ITemplateContentAppService _appService;

		public TemplateContentController(ITemplateContentAppService appService)
		{
			this._appService = appService;
		}

		[HttpGet]
		public virtual async Task<TextTemplateContentDto> GetAsync(GetTemplateContentInput input)
		{
			return await this._appService.GetAsync(input);
		}

		[HttpPut]
		[Authorize("TextTemplateManagement.TextTemplates.EditContents")]
		[Route("restore-to-default")]
		public virtual async Task RestoreToDefaultAsync(RestoreTemplateContentInput input)
		{
			await this._appService.RestoreToDefaultAsync(input);
		}

		[HttpPut]
		public virtual async Task<TextTemplateContentDto> UpdateAsync(UpdateTemplateContentInput input)
		{
			return await this._appService.UpdateAsync(input);
		}
	}
}
